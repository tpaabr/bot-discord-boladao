const Discord = require("discord.js");
const client = new Discord.Client();
const fs = require("fs");
const util = require('util');
const audioconcat = require('audioconcat')
const config = require("./config.json");
const count = { }
const audioCounter = { }
const ffmpegPath = require('@ffmpeg-installer/ffmpeg').path;
const ffmpeg = require('fluent-ffmpeg');
ffmpeg.setFfmpegPath(ffmpegPath);
const { exec } = require("child_process");


function generateOutputFile(channel, member) {
    const fileName = `./recordings`;
    return fs.createWriteStream(fileName);
  }

const messageTable = {
    "%commandNotFound": async (message, param) => {
        let resp = await message.channel.send(`??${param}??`);
    },
    "%oi": async (message, param) => {
        if(param === null){
            let resp = await message.channel.send("Oi?!");
            resp.edit(`Oiii ${message.author.username}`) ;
        }else{
            let resp = await message.channel.send("Oi?!");
            resp.edit(`"${param}" éeeee ${message.author.username}`) ;
        }
    },
    "%entrarVoice": async(message, param) => {
        let authorVoiceChannel = message.member.voice.channel;
        if(authorVoiceChannel != null){
            let resp = await message.channel.send("To entrando.");
            let currDate = new Date();

            authorVoiceChannel.join().then(connection => {

                const dispatcher = connection.play('sirene.mp3');

                connection.on('speaking', (user, speaking) => {
                    let dir = `recordings_${user.username.toString()}`;
                    if(speaking){
                        //Criar um diretorio se não existir para esse usuario
                        if (!fs.existsSync(dir)){
                            fs.mkdirSync(dir);
                        }
                        if (audioCounter[user.username.toString()] == null) {
                            audioCounter[user.username.toString()] = 0;
                        }
                        audioCounter[user.username] += 1
                        console.log(`${user.username} esta falando no servidor: ${message.guild.name}` );
                        const audio = connection.receiver.createStream(user, { mode: 'pcm' });
                        audio.pipe(fs.createWriteStream(`${dir}/${audioCounter[user.username]}_day${currDate.getDate().toString()}_month${(currDate.getMonth() + 1).toString()}_year${currDate.getUTCFullYear().toString()}.pcm`));
                    }

                });
            }).catch(console.log);
        }else{
            let resp = await message.channel.send("Não tem canal.");
        }
    },
    "%sairVoice": async(message, param) => {
        console.log(`Desligaram meus ouvidos no servidor ${message.guild.name}`)
        let authorVoiceChannel = message.member.voice.channel;
        authorVoiceChannel.leave();
    },
    "%processarAudio": async (message, param) => {
        var pcmFiles = []
        
        try {
            //Testar se não existe parametro:
            if(param == null){
                let resp = await message.channel.send("De qual usuario?")
            }else{
                let targetUser
                if(param === "meu"){
                    
                    targetUser = await message.author.username
                    
                }else{
                    targetUser = await param.match(/([a-zA-Z0-9]*)/)[1]
                }
                
                let dir = `recordings_${targetUser}`
                let dirMp3 = `mp3_${targetUser}`
    
                //Testar se possui gravação
                if (!fs.existsSync(dir)){
                    let resp = await message.channel.send(`Não possuo gravações de ${targetUser}, verifique se o nome esta certo`)
                }else{
                    console.log(`Estão solicitandos meus audios no servidor: ${message.guild.name}`)
                    //Tudo esta certo:
                    let resp = await message.channel.send(`To processando os arquivos do usuario: ${targetUser}`);
                    //Criar lista com todos os arquivos dentro da pasta do usuario
                    pcmFiles = fs.readdirSync(dir);
                    //Criando um array para ordenar
                    let novaPcm = []
                    pcmFiles.forEach(element => {
                        novaPcm.push(element);
                    });

                    let currDate = new Date();
                    
                    //Adicionando diretorio ao nome
                    for (let i = 0; i < novaPcm.length; i++) {
                        value = novaPcm[i]
                        novaPcm[i] = dir + "/" + value;
                    }
                    //Criando pasta para guardar wav
                    if (!fs.existsSync(`${dirMp3}`)){
                        fs.mkdirSync(`${dirMp3}`);
                    }

                    let countMp3 = 0
                    //Convertendo em MP3
                    for (let i = 0; i < novaPcm.length; i++) {
                        let element = novaPcm[i];
                        exec(`ffmpeg -f s32le -ar 48000 -ac 1 -i ${element} ${dirMp3}/${countMp3}_day${currDate.getDate().toString()}_month${(currDate.getMonth() + 1).toString()}_year${currDate.getUTCFullYear().toString()}.mp3`, (err, stdout, stderr) => {
                            //Resolvendo erros
                            if(err){
                                console.log(err)
                                return
                            }
                            if(stdout){
                                console.log(stdout)
                                return
                            }
                            if(stderr){
                                console.log(stderr)
                                return
                            }
                        })
                        countMp3 += 1;
                    }
                    //Pegando arquivos Wav
                    mp3Files = fs.readdirSync(dirMp3);

                    //Criando um array para ordenar
                    let novaMp3 = []
                    mp3Files.forEach(element => {
                        novaMp3.push(element);
                    });

                    novaMp3.sort((a, b) => {
                        nameA = parseInt(a.match(/([0-9]*)_/)[1]);
                        nameB = parseInt(b.match(/([0-9]*)_/)[1]);
                        return nameA - nameB;
                    });

                    for (let i = 0; i < novaMp3.length; i++) {
                        value = novaMp3[i]
                        novaMp3[i] = dirMp3 + "/" + value;
                    }


                    //Concatenar não funcionando
                    //Usando modulo audioConcat
                    let outputFile = `audiosFinais/${targetUser}_day${currDate.getDate().toString()}_month${(currDate.getMonth() + 1).toString()}_year${currDate.getUTCFullYear().toString()}.mp3`
                    audioconcat(novaMp3)
                    .concat(outputFile)
                    .on('start', (command) => {
                        console.log(`Processo começou, concatenação dos arquivos mp3 do ${targetUser}`);
                        //console.log("Processo começou:", command);
                    })
                    .on('error', (err) => {
                        console.log(err);
                        message.channel.send("Houve algum erro");
                    })
                    .on('end', function (output) {
                        console.error(`Arquivo criado em: ${outputFile}`);
                    })
                    let result = await message.channel.send("Tenho isso:", { files: [outputFile] });
                }
            }
        }catch(e){
            console.log(e);
        }
    },
    "%emojiOn": async(message, param) => {
        
    },
    "%help": async (message, param) => {
        let listaComandos = ""
        for (const key in messageTable) {
            if(key != "%comandNotFound"){
                listaComandos += key + " / ";
            }
        }
        let resp = await message.channel.send("Oi?!");
        resp.edit(`Ai:
        ${listaComandos}`) ;
    }
}

const countElements = (obj) => {
    return Object.keys(obj).length;
}

client.once("ready", () => {
    let numberOfGuilds = countElements(client.guilds);
    let numberOfChannels = countElements(client.channels);
    let numberOfUsers = countElements(client.users);

    console.log(`Bot started with ${numberOfUsers} users, in ${numberOfChannels} channels, and ${numberOfGuilds} servers`);
    client.user.setActivity(`EAE galeeeeeeeeeeeeeeeera`);
});

client.on("guildCreate", (guild) => {
    console.log(`Este servidor me adicionou: ${guild.name} id: ${guild.id} users: ${guild.memberCount}`);
    
});

client.on("guildDelete", (guild) => {
    console.log(`Eu fui excluido deste servidor: ${guild.name} id: ${guild.id}`);
});

client.on("message", async message => {
    //Utilizando HashTable para guardar comandos de mensagem
    if(message.author.bot) return;

    //Tentando capturar comandos
    let tryOrder = await message.content.match(/(%[a-zA-Z]*)[\s]?/);
    let tryOrderParam = await message.content.match(/[\s]+([a-zA-Z0-9]*)/);
    let currOrder
    //Testando se foi mandado um comando com argumento ou sem
    if(tryOrder != null){
        tryOrder = tryOrder[1];
        currOrder = messageTable[tryOrder];
    };
    if(tryOrderParam != null){
        tryOrderParam = tryOrderParam[1];
    };
    
    //Rodando função
    if(currOrder != null){
        currOrder(message, tryOrderParam);
    }else{
        if(tryOrder != null){
            currOrder = messageTable["%commandNotFound"];
            currOrder(message, tryOrder);
        }
    }

});

client.login(config.token);
